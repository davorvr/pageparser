# PageParser

Provides a quick and easy interface around BeautifulSoup for scraping a specific parameter from webpages.

## Installation

The package can be download from the [Releases page](https://gitlab.com/davorvr/pageparser/-/releases).

Linux: 

```shell
pip install /home/user/path/to/package/pageparser-0.0.1-py3-none-any.whl
```

Windows:

```
pip install C:\Users\user\path\to\package\pageparser-0.0.1-py3-none-any.whl
```

## Usage

Let's say we want to grab the `Time to first decision` parameter from [Neuropharmacology journal's website](https://www.journals.elsevier.com/neuropharmacology). A quick inspection of the element in any popular web browser yields the following HTML tag:

```html
<a href="javascript:;" class="tooltip" role="button" aria-describedby="tooltipTtfdWeeksTitle" 
aria-controls="publicationMetricTooltipTtfdWeeks" rel="external">
	<b>3.2 weeks</b>
	<span class="icon">ℹ</span>
</a>
```

The `aria-describedby` attribute's value `"tooltipTtfdWeeksTitle"` is possibly specific to the piece of information we're looking to extract (`3.2 weeks`). So let's use PageParser to get it. First, we have to import the PageParser class.

```python
from pageparser import PageParser
```

Then, let's instantiate it with our attribute-value pair as a dictionary, and run the `parse` method on the URL of interest (`parse` also accepts a second, optional `features` argument which is passed verbatim to the BeautifulSoup constructor, and defaults to `lxml`):

```python
>>> attribute = {"aria-describedby":"tooltipTtfdWeeksTitle"}
>>> p = PageParser(attribute)
>>> p.parse("https://www.journals.elsevier.com/neuropharmacology", "lxml")
[<a aria-controls="publicationMetricTooltipTtfdWeeks" aria-describedby="tooltipTtfdWeeksTitle" class="tooltip" href="javascript:;" role="button">
 <b>3.2 weeks</b>
 <span class="icon">ℹ</span>
 </a>]
```

We got a list of matching tags containing exactly one tag, the one we're interested in. Now we can write a short parsing function and pass it to a new instance of PageParser:

```python
>>> parser = lambda x: x.findChildren()[0].text
>>> p = PageParser(attribute, parser)
```

Calling the `parse` method now gives us exactly what we need:

```python
>>> p.parse("https://www.journals.elsevier.com/neuropharmacology")
['3.2 weeks']
```

We can now use this instance in a loop. Let's say we have a list of journal dicts:

```python
>>> journals = [
...     {"name": "Neuropharmacology",
...      "url": "https://www.journals.elsevier.com/neuropharmacology"},
...     {"name": "European Journal of Vascular and Endovascular Surgery",
...      "url": "https://www.journals.elsevier.com/european-journal-of-vascular-and-endovascular-surgery"}
... ]   
```

We can easily append this new piece of information for each journal and export the updated list to a JSON file:

```python
>>> for j in journals:
...     j["time to first decision"] = p.parse(j["url"])
>>> journals
[{'name': 'Neuropharmacology',
  'url': 'https://www.journals.elsevier.com/neuropharmacology',
  'time to first decision': ['3.2 weeks']},
 {'name': 'European Journal of Vascular and Endovascular Surgery',
  'url': 'https://www.journals.elsevier.com/european-journal-of-vascular-and-endovascular-surgery',
  'time to first decision': ['3.5 weeks']}]
>>> import json
>>> with open("journals.json", "w") as f:
...     json.dump(journals, f)
```
