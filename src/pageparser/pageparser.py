import requests
import bs4
#import json
from typing import Callable, Tuple, List, Sequence
from inspect import getsource
import re

class PageParser:
    def __init__(self, attrs: dict, parser: Callable[[bs4.element.Tag], str] = lambda x: x):
        self._bs_attrs = attrs
        self._parser_func = parser

        self._url = None
        self._response = None
        self._source = None
        self._items = list()
        self._parsed = list()

    @property
    def bs_attrs(self):
        return self._bs_attrs
    @bs_attrs.setter
    def bs_attrs(self, value: str):
        self._bs_attrs = value
    @bs_attrs.deleter
    def bs_attrs(self):
        self._bs_attrs = None

    @property
    def parser(self):
        return getsource(self._parser_func).rstrip().split(" = ")[1]
    @parser.setter
    def parser(self, func: Callable[[bs4.element.Tag], str]):
        self._parser_func = func
    @parser.deleter
    def parser(self):
        self._parser_func = lambda x: x

    @property
    def url(self):
        return self._url
    @url.setter
    def url(self, value: str):
        if not re.match("^https?://", value):
            value = "https://"+value
        self._url = value
    @url.deleter
    def url(self):
        self._url = None

    def _get_page(self, url: str, features: str) -> Tuple[requests.models.Response, bs4.BeautifulSoup]:
        r = requests.get(url)
        r.raise_for_status()
        s = bs4.BeautifulSoup(r.text, features=features)
        return r, s

    def _get_attrs(self, s: bs4.BeautifulSoup, attrs: dict) -> List[bs4.element.Tag]:
        result = s.find_all(attrs=attrs)
        return result

    def _run_parser(self, p: Callable[[bs4.element.Tag], str], items: Sequence) -> List[str]:
        return [ p(x) for x in items ]

    def parse(self, url: str = None, features: str = "lxml"):
        if not url:
            if not self.url:
                raise NameError("No URL specified.")
        else:
            self.url = url
        self._response, self._source = self._get_page(self._url, features)
        self._items = self._get_attrs(self._source, self._bs_attrs)
        self._parsed = self._run_parser(self._parser_func, self._items)
        return self._parsed
